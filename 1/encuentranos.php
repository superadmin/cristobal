<?php require "code-login.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<title>Donde Estamos</title>
	<link rel="stylesheet" href="css/estilos.css">
	<link rel="stylesheet" href="css/estilazos.css">	
	<link rel="stylesheet" href="css/nuevos-estilos.css">		
	<meta http-equiv="Content-Type" content="text/html;" charset="utf-8"/>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximun-scale?=1.0">
	<script src="https://kit.fontawesome.com/f6540b2b09.js" crossorigin="anonymous"></script>
</head>

<body>
	<header class="titulo">
		<h1>Clinica Sonrie</h1>		
	</header>		

			<ul class="menu">
			<li><a href="index.php">Inicio</a></li>
			<li><a href="encuentranos.php">Encuentranos</a></li>
			<li><a href="dentistas.php">Nuestros Dentistas</a></li>
			<li><a href="trabajos.php">Nuestros Trabajos</a></li>			
			<li><a href="reservar.php">Reservar Hora</a></li>
			<li><a href="inicio-sesion.php"><i class="fas fa-user" ></i></a></li>	
	</ul>	


	<div class="container-all">
		<div class="cnt-form"> 

			<img src="img/descarga.png"  alt="" class="logo">
			<h1 class="title">Encuentranos en San Martin #547, Rancagua </h1>
			<h1 class="title">Contacto +56987532544</h1>

			
		</div>



		<div class="ctn-text2" link="https://www.google.cl/maps/@-34.1688026,-70.7458449,19.89z">
			<div class="capa"></div>
			<h1 class="title-description"></h1>
			<p class="text-description">
				<a href="https://www.google.cl/maps/@-34.1688026,-70.7458449,19.89z"></a>
			</p>
		</div>
	</div>
	
<br>
    <br>
    <br>
    <footer class="footer">
        <div class="container-fot">
            <div class="row-fot">
                <div class="footer-col">
                    <h4>Clinica Sonrie</h4>
                    <ul>
                        <li><a href="#" target="_blank">Encuentranos</a></li>
                        <li><a href="#">Nuestros Dentistas</a></li>
                        <li><a href="#">Reservar Hora</a></li>
                    </ul>
                </div>

                <div class="footer-col">
                    <h4>Redes Sociales</h4>
                    <ul>
                        <li><a href="#" target="_blank">Pruebas</a></li>
                    </ul>
                </div>


            </div>
            <br>
                <div class="divsitio">
                    <h4 class="sitio_des">2020 - 2021. Sitio Desarrollado por BrandMans</h4>
                </div>
            </div>

    </footer>
	
</body>
</html>
