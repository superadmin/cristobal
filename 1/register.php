<?php require 'code-register.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<title>Registrarse</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/estilos.css">
	<meta name="viewport" content="width=device-width, user-scalable=no,
	initial-scale=1.0, maximun-scale=1.0, minimun-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
	<div class="container-all">
		<div class="cnt-form">
			<img src="img/descarga.PNG" alt="" class="logo">
			<h1 class="title">Registrarse</h1>
			<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post"> 

				<label for="username">Nombre de usuario</label>
				<input type="text" id="username" name="username">
				<span class="msg-error"><?php echo $username_err; ?></span>
				<label for="email">Email</label>
				<input type="text" name="email" id="email">
				<span class="msg-error"><?php echo $email_err; ?></span>
				<label for="password">Contraseña</label>
				<input type="password" name="password" id="password">
				<span class="msg-error"><?php echo $password_err; ?></span>

				<input type="submit" value="Registrarse">
			</form>

			<span class="text-footer"> ¿Ya te has registrado?
				<a href="inicio-sesion.php">Iniciar Sesión</a>
			</span>
		</div>

		<div class="ctn-text">
			<div class="capa"></div>
			<h1 class="title-description">Clinica Sonrie</h1>
			<h3 class="title-description">Nuestra Clinica</h3>
			

		</div>

	</div>

</body>
</html>
