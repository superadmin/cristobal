-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 11-10-2021 a las 00:42:13
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `login_tuto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargos`
--

CREATE TABLE IF NOT EXISTS `cargos` (
  `id_car` int(2) NOT NULL AUTO_INCREMENT,
  `nom_car` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_car`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `cargos`
--

INSERT INTO `cargos` (`id_car`, `nom_car`) VALUES
(1, 'Secretaria'),
(2, 'Doctor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `clave` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `email`, `clave`) VALUES
(16, 'dadfwasda', 'dasdsadas', 'e3ccc6a8391981626c2a16626ef9adc7'),
(17, 'dsasdada', 'da', '6d2cf3fdab44bdfc1990230f21e4c25d'),
(18, 'yo2', 'yo2', '25f9e794323b453885f5181f1b624d0b'),
(19, 'cristobal', 'cris.master.14@gmail.com', '25f9e794323b453885f5181f1b624d0b'),
(20, 'prueba', 'prueba@gmail.com', '25f9e794323b453885f5181f1b624d0b'),
(21, 'prueba3', 'prueba3@gmail.com', '25f9e794323b453885f5181f1b624d0b'),
(22, 'ejemplo', 'ejemplo@gmail.com', '25f9e794323b453885f5181f1b624d0b'),
(23, 'prueba1000', 'fbhewbfyjhwe', '25f9e794323b453885f5181f1b624d0b'),
(24, 'hjvbcehjsvbfhjes', 'ejemplo1@gmail.com', '25f9e794323b453885f5181f1b624d0b'),
(25, 'ejem', 'ejem@gmail.com', '25f9e794323b453885f5181f1b624d0b'),
(26, 'juan', 'juan@gmail.com', '25f9e794323b453885f5181f1b624d0b'),
(28, 'cris', 'cris123456', '25f9e794323b453885f5181f1b624d0b'),
(29, '123456', '123456', 'e10adc3949ba59abbe56e057f20f883e'),
(30, 'ejemplo12', 'ejemplo12@gmail.com', 'e10adc3949ba59abbe56e057f20f883e'),
(31, 'cristpher', 'cristopher@gmail.com', 'e10adc3949ba59abbe56e057f20f883e'),
(32, 'cristoball', '123456789123', 'df96220fa161767c5cbb95567855c86b'),
(33, 'prueba8', 'prueba8@gmail.com', '25f9e794323b453885f5181f1b624d0b'),
(34, 'dentista1', 'dentista1@gmail.com', '25f9e794323b453885f5181f1b624d0b'),
(35, 'secretaria1', 'secretaria1@gmail.com', '25f9e794323b453885f5181f1b624d0b');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
