-- -------------------------------------------------------------
-- TablePlus 4.2.0(388)
--
-- https://tableplus.com/
--
-- Database: login_tuto
-- Generation Time: 2021-10-11 20:29:08.6240
-- -------------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


CREATE TABLE `cargos` (
  `id_car` int NOT NULL AUTO_INCREMENT,
  `nom_car` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_car`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `grupos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(12) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `usuarios` (
  `id` int NOT NULL AUTO_INCREMENT,
  `usuario` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `clave` varchar(200) NOT NULL,
  `grupo_id` bigint unsigned DEFAULT NULL,
  `rol` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

INSERT INTO `cargos` (`id_car`, `nom_car`) VALUES
(1, 'Secretaria'),
(2, 'Doctor');

INSERT INTO `grupos` (`id`, `nombre`) VALUES
(1, 'Grupo 1'),
(2, 'Grupo 2'),
(3, 'Grupo 3');

INSERT INTO `usuarios` (`id`, `usuario`, `email`, `clave`, `grupo_id`, `rol`) VALUES
(16, 'dadfwasda', 'dasdsadas', 'e3ccc6a8391981626c2a16626ef9adc7', 0, NULL),
(17, 'dsasdada', 'da', '6d2cf3fdab44bdfc1990230f21e4c25d', 0, NULL),
(18, 'yo2', 'yo2', '25f9e794323b453885f5181f1b624d0b', 0, NULL),
(19, 'cristobal', 'cris.master.14@gmail.com', '25f9e794323b453885f5181f1b624d0b', 0, NULL),
(20, 'prueba', 'prueba@gmail.com', '25f9e794323b453885f5181f1b624d0b', 0, NULL),
(21, 'prueba3', 'prueba3@gmail.com', '25f9e794323b453885f5181f1b624d0b', 0, NULL),
(22, 'ejemplo', 'ejemplo@gmail.com', '25f9e794323b453885f5181f1b624d0b', 0, NULL),
(23, 'prueba1000', 'fbhewbfyjhwe', '25f9e794323b453885f5181f1b624d0b', 0, NULL),
(24, 'hjvbcehjsvbfhjes', 'ejemplo1@gmail.com', '25f9e794323b453885f5181f1b624d0b', 0, NULL),
(25, 'ejem', 'ejem@gmail.com', '25f9e794323b453885f5181f1b624d0b', 0, NULL),
(26, 'juan', 'juan@gmail.com', '25f9e794323b453885f5181f1b624d0b', 0, NULL),
(28, 'cris', 'cris123456', '25f9e794323b453885f5181f1b624d0b', 0, NULL),
(29, '123456', '123456', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL),
(30, 'ejemplo12', 'ejemplo12@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL),
(31, 'cristpher', 'cristopher@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL),
(32, 'cristoball', '123456789123', 'df96220fa161767c5cbb95567855c86b', 0, NULL),
(33, 'prueba8', 'prueba8@gmail.com', '25f9e794323b453885f5181f1b624d0b', 0, NULL),
(34, 'dentista1', 'dentista1@gmail.com', '25f9e794323b453885f5181f1b624d0b', 1, 'dentista'),
(35, 'secretaria1', 'secretaria1@gmail.com', '25f9e794323b453885f5181f1b624d0b', 1, 'secretaria');



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;