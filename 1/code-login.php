<?php

// Inicializar la sesión
session_start();

require_once "conexion.php";

// Inicializar variables vacías...
$email = '';
$password = '';

// Inicializar variables para controlar errores
$email_err = '';
$password_err = '';

if($_SERVER["REQUEST_METHOD"] === "POST"){

    if(empty($_POST["email"])){
        $email_err = "Por favor, Ingrese el correo electronico";
    }else{
        $email = trim($_POST["email"]);
    }

    if(empty($_POST["password"])){
        $password_err = "Por favor, Ingrese la contraseña";
    }else{
        $password = trim($_POST["password"]);
    }

    // Validar credenciales
    if(empty($email_err) && empty($password_err)){

        $sql ="SELECT id, usuario ,email ,clave FROM usuarios WHERE email =?";

        if($stmt = mysqli_prepare($link, $sql)){
            mysqli_stmt_bind_param($stmt, "s", $param_email);

            $param_email = $email;

            if(mysqli_stmt_execute($stmt)){
                mysqli_stmt_store_result($stmt);
            }

            if(mysqli_stmt_num_rows($stmt) == 1){
                mysqli_stmt_bind_result($stmt, $id, $usuario, $email, $password_bd);
                if(mysqli_stmt_fetch($stmt)){
                    if(md5($password) == $password_bd){
                        // Almacenar datos en variables de session
                        $_SESSION["loggedin"] = true;
                        $_SESSION["id"] = $id;
                        $_SESSION["email"] = $email;

                        header("location: bienvenida.php");
                    }else{
                        $password_err = "La contraseña no es valida";
                    }
                }else{
                    header("location: no-correo.php");
                    
                }
            }else{
               header("location: no-correo.php");

            }
        }
    }

    mysqli_close($link);

}
