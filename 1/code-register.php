<?php

require_once "conexion.php";

// Inicializar variables vacías...
$username = '';
$email = '';
$password = '';

// Inicializar variables para controlar errores
$username_err = '';
$email_err = '';
$password_err = '';

if($_SERVER["REQUEST_METHOD"] == "POST"){

    // Validar input de nombre usuario
    if(empty($_POST["username"])){
        $username_err = "Por favor, ingrese un nombre de usuario";
    }else{
        // Preparando una declaracion de selecion
        $sql = "SELECT id FROM usuarios WHERE usuario = ?";

        if($stmt = mysqli_prepare($link, $sql)){
            mysqli_stmt_bind_param($stmt, "s" , $param_username);

            $param_username = trim($_POST["username"]);

            if(mysqli_stmt_execute($stmt)){
                mysqli_stmt_store_result($stmt);

                if(mysqli_stmt_num_rows($stmt) == 1){
                    $username_err = "Este nombre de usuario ya esta registrado";
                }else{
                    $username = trim($_POST["username"]);
                }
            }
        }
    }

    // Validar input de email
    if(empty($_POST["email"])){
        $email_err = "por favor, ingrese un correo de usuario";
    }else{
        // Preparando una declaracion de selecion
        $sql = "SELECT id FROM usuarios WHERE email = ?";

        if($stmt = mysqli_prepare($link,$sql)){
            mysqli_stmt_bind_param($stmt,"s",$param_email);

            $param_email = trim($_POST["email"]);

            if(mysqli_stmt_execute($stmt)){
                mysqli_stmt_store_result($stmt);

                if(mysqli_stmt_num_rows($stmt) == 1){
                    $email_err = "Este correo  ya esta registrado";
                }else{
                    $email = trim($_POST["email"]);
                }
            }
        }
    }

    // Validar contraseña
    if(empty($_POST["password"])){
        $password_err = "por favor , ingrese contraseña";
    }elseif(strlen(trim($_POST["password"])) <4 ){
        $password_err = "la contraseña debe tener 4 caracteres";
    }else{
        $password = trim($_POST["password"]);
    }

    // Comprobando los errores de entrada antes de insertar los datos en la base de datos
    if(empty($username_err) && empty($email_err) && empty($password_err)){
        $sql = "INSERT INTO usuarios (usuario, email, clave) VALUES (?, ?, ?)";

        if($stmt = mysqli_prepare($link, $sql)){
            mysqli_stmt_bind_param($stmt, "sss" , $param_username ,$param_email ,$param_password);

            // Establecer parámetros
            $param_username = $username;
            $param_email    = $email;
            $param_password = md5($password);

            if(mysqli_stmt_execute($stmt)){
                header("location: index.php");
            }else{
                echo "Algo salio mal , intentalo despues";
            }
        }
    }

    mysqli_close($link);

}
