<?php require "code-login.php"; 	
  header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
 	header("Expires: Sat, 1 Jul 2000 05:00:00 GMT"); // Fecha en el pasado	
?>

<!DOCTYPE html>
<html>
<head>
	<title>Profecionales</title>
	<link rel="stylesheet" href="css/estilos.css">
	<link rel="stylesheet" href="css/estilazos.css">
	<link rel="stylesheet" href="css/estilazoz.css">	
	<link rel="stylesheet" href="css/nuevos-estilos.css">		
	<script src="https://kit.fontawesome.com/f6540b2b09.js" crossorigin="anonymous"></script>
	<meta http-equiv="Content-Type" content="text/html;" charset="utf-8"/>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximun-scale?=1.0">
  	<meta http-equiv="Expires" content="0">
  	<meta http-equiv="Last-Modified" content="0">
  	<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
  	<meta http-equiv="Pragma" content="no-cache">	
</head>

<body>
	<header class="titulo">
		<h1>Clinica Sonrie</h1>		
	</header>		

			<ul class="menu">
			<li><a href="index.php">Inicio</a></li>
			<li><a href="encuentranos.php">Encuentranos</a></li>
			<li><a href="dentistas.php">Nuestros Dentistas</a></li>
			<li><a href="trabajos.php">Nuestros Trabajos</a></li>
			<li><a href="reservar.php">Reservar Hora</a></li>
			<li><a href="inicio-sesion.php"><i class="fas fa-user" ></i></a></li>	
	</ul>	
<br>

		<ul class="menu">
			<li><a><h1>Nuestros Profesionales</h1></a></li>
		</ul>


	<div class="container-all">
		<div class="cnt-form"> 

			<img src="img/icono_usuario.jpg" alt="" class="logo">
			<h1 class="title">Juan Carlos Quinteros</h1>
			<h1 class="title">Odontologo</h1>

			
		</div>

		<div class="ctn-text22">
			<div class="capa"></div>
			<h1 class="title-description"></h1>
			<p class="text-description">
				<a href="reservar.php"></a>
			</p>
		</div>
	</div>


		<div class="container-all">
		<div class="cnt-form"> 

			<img src="img/icono_usuario.jpg" alt="" class="logo">
			<h1 class="title">Pablo Olate </h1>
			<h1 class="title">Ortodoncista</h1>

			
		</div>
			
		<div class="ctn-text22">
			<div class="capa"></div>
			<h1 class="title-description"></h1>
			<p class="text-description">
				<a href="reservar.php"></a>
			</p>
		</div>
	</div>


		<div class="container-all">
		<div class="cnt-form"> 

			<img src="img/icono_usuario.jpg" alt="" class="logo">
			<h1 class="title">Cristian Guzman </h1>
			<h1 class="title">Dr. Maxilofacial</h1>

			
		</div>

		<div class="ctn-text22">
			<div class="capa"></div>
			<h1 class="title-description"></h1>
			<p class="text-description">
				<a href="reservar.php"></a>
			</p>
		</div>
	</div>


		<div class="container-all">
		<div class="cnt-form"> 

			<img src="img/icono_usuario.jpg" alt="" class="logo">
			<h1 class="title">Tesa Perez </h1>
			<h1 class="title">Odontopediatra</h1>

			
		</div>

		<div class="ctn-text22">
			<div class="capa"></div>
			<h1 class="title-description"></h1>
			<p class="text-description">
				<a href="reservar.php"></a>
			</p>
		</div>
	</div>
	
<br>
    <br>
    <br>
    <footer class="footer">
        <div class="container-fot">
            <div class="row-fot">
                <div class="footer-col">
                    <h4>Clinica Sonrie</h4>
                    <ul>
                        <li><a href="encuentranos.php" target="_blank">Encuentranos</a></li>
                        <li><a href="#">Nuestros Dentistas</a></li>
                        <li><a href="#">Reservar Hora</a></li>
                    </ul>
                </div>

                <div class="footer-col">
                    <h4>Redes Sociales</h4>
                    <ul>
                        <li><a href="#" target="_blank">Pruebas</a></li>
                    </ul>
                </div>


            </div>
            <br>
                <div class="divsitio">
                    <h4 class="sitio_des">2020 - 2021. Sitio Desarrollado por BrandMans</h4>
                </div>
            </div>

    </footer>
	
</body>
</html>
