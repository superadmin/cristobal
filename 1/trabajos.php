<?php require "code-login.php"; 
  header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
  header("Expires: Sat, 1 Jul 2000 05:00:00 GMT"); // Fecha en el pasado
?>
<!DOCTYPE html>
<html>
<head>
	<title>Trabajos</title>
	<link rel="stylesheet" href="css/estilos.css">
	<link rel="stylesheet" href="css/estilazos.css">		
	<link rel="stylesheet" href="css/nuevos-estilos.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<script src="https://kit.fontawesome.com/f6540b2b09.js" crossorigin="anonymous"></script>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximun-scale?=1.0">
</head>


<body>
	<header class="titulo">
		<h1>Clinica Sonrie</h1>
	</header>

	<ul class="menu">
		<li><a href="index.php">Inicio</a></li>
		<li><a href="Encuentranos.php">Encuentranos</a></li>
		<li><a href="dentistas.php">Nuestros Dentistas</a></li>
		<li><a href="reservar.php">Reservar Hora</a></li>
		<li><a href="trabajos.php">Nuestros Trabajos</a></li>
		<li><a href="inicio-sesion.php"><i class="fas fa-user" ></i></a></li>	
	</ul>
	<div class="container-all">
		<div class="cnt-form"> 

			<img src="img/implantes.png" alt="" class="logo">
			<h1 class="title">Implantes</h1>
			<h3 class="title">Con más pacientes atendidos en tratamientos de implantología.</h3>

		</div>


		<div class="cnt-form"> 

			<img src="img/vanguardia.png" alt="" class="logo">
			<h1 class="title">Vanguardia</h1>
			<h3 class="title">Se utilizan las mejores tecnologias disponibles de los mejores estandares a nivel regional.</h3>

		</div>
				<div class="cnt-form"> 

			<img src="img/experiencia.png" alt="" class="logo">
			<h1 class="title">Experiencia</h1>
			<h3 class="title">Contamos con profesionales calificados para entregar el mejor servicio.</h3>

		</div>
				<div class="cnt-form"> 

			<img src="img/ortodoncia.png" alt="" class="logo">
			<h1 class="title">Ortodoncia</h1>
			<h3 class="title">Cada día aplicamos más trataminetos de ortodoncia con los mejores estandares y precios competitivos del mercado.</h3>

		</div>
 </div>		

<br>
    <br>
    <br>
    <footer class="footer">
        <div class="container-fot">
            <div class="row-fot">
                <div class="footer-col">
                    <h4>Clinica Sonrie</h4>
                    <ul>
                        <li><a href="#" target="_blank">Encuentranos</a></li>
                        <li><a href="#">Nuestros Dentistas</a></li>
                        <li><a href="#">Reservar Hora</a></li>
                    </ul>
                </div>

                <div class="footer-col">
                    <h4>Redes Sociales</h4>
                    <ul>
                        <li><a href="#" target="_blank">Pruebas</a></li>
                    </ul>
                </div>


            </div>
            <br>
                <div class="divsitio">
                    <h4 class="sitio_des">2020 - 2021. Sitio Desarrollado por BrandMans</h4>
                </div>
            </div>

    </footer>


</body>
</html>
