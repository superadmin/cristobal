<!DOCTYPE html>
<html>
<head>
    <title>Sesión cerrada</title>
    <link rel="stylesheet" href="css/estilos.css">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximun-scale?=1.0">
</head>
    <body class="error">

       <div class="ctn-welcome">
        
        <img src="img/descarga.png" class="logo-wemcome">
        <h1 class="title-welcome">Sesión cerrada exitosamente</h1>
        
        <a href="index.php" class="close-sesion">Volver al inicio</a>

       </div>

</body>
</html>
