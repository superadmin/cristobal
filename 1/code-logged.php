<?php

// Inicializar la sesión
session_start();

if (! isset($_SESSION['loggedin'])) {

    require_once 'sesion-no-iniciada.php';
    die();

}

