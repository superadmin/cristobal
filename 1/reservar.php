<?php require "code-login.php"; 
  header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
  header("Expires: Sat, 1 Jul 2000 05:00:00 GMT"); // Fecha en el pasado
?>
<!DOCTYPE html>
<html>
<head>
	<title>Reservar</title>
	<link rel="stylesheet" href="css/estilos.css">
	<link rel="stylesheet" href="css/estilazos.css">	
	<link rel="stylesheet" href="css/nuevos-estilos.css">
	<script src="https://kit.fontawesome.com/f6540b2b09.js" crossorigin="anonymous"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximun-scale?=1.0">
</head>


<body>
	<header class="titulo">
		<h1>Clinica Sonrie</h1>
	</header>

	<ul class="menu">
		<li><a href="index.php">Inicio</a></li>
		<li><a href="Encuentranos.php">Encuentranos</a></li>
		<li><a href="dentistas.php">Nuestros Dentistas</a></li>
		<li><a href="reservar.php">Reservar Hora</a></li>
		<li><a href="trabajos.php">Nuestros Trabajos</a></li>
		<li><a href="inicio-sesion.php"><i class="fas fa-user" ></i></a></li>			
	</ul>
	<div class="container-all">
		<div class="cnt-form"> 

			<img src="img/descarga.png" alt="" class="logo">
			<h1 class="title">Contactanos via WhatsApp</h1>
			<h2 class="title">+56973250854</h2>
			<h3 class="title">Una de las vias mas rapidas y seguras</h3>

		</div>


		<div class="cnt-form"> 

			<img src="img/what.png" alt="" class="logo">
			<h1 class="title">Con el Link Directo</h1>

			<link rel="stylesheet"
      href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"
      integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ=="
      crossorigin="anonymous">
<a href="https://api.whatsapp.com/message/GF5N5TIDYZ25F1" class="btn btn-success"> Click Aqui</a>	
			<h3 class="title">O</h3>
			<h3 class="title">Escanea el Codigo QR</h3>

		</div>

	
		<div class="ctn-text30">
			<div class="capa"></div>
			</form>
		</div>

	</div>
<br>
    <br>
    <br>
    <footer class="footer">
        <div class="container-fot">
            <div class="row-fot">
                <div class="footer-col">
                    <h4>Clinica Sonrie</h4>
                    <ul>
                        <li><a href="#" target="_blank">Encuentranos</a></li>
                        <li><a href="#">Nuestros Dentistas</a></li>
                        <li><a href="#">Reservar Hora</a></li>
                    </ul>
                </div>

                <div class="footer-col">
                    <h4>Redes Sociales</h4>
                    <ul>
                        <li><a href="#" target="_blank">Pruebas</a></li>
                    </ul>
                </div>


            </div>
            <br>
                <div class="divsitio">
                    <h4 class="sitio_des">2020 - 2021. Sitio Desarrollado por BrandMans</h4>
                </div>
            </div>

    </footer>

</body>
</html>
