<?php require "code-login.php"; 
  header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
  header("Expires: Sat, 1 Jul 2000 05:00:00 GMT"); // Fecha en el pasado
?>

<!DOCTYPE html>
<html>
<head>
	<title>Home</title>

	<link rel="stylesheet" href="css/estilos.css">
	<link rel="stylesheet" href="css/nuevos-estilos.css">
	<link rel="stylesheet" href="css/estilazos.css">		
	<meta http-equiv="Content-Type" content="text/html;" charset="utf-8"/>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximun-scale?=1.0">
	<meta http-equiv="Expires" content="0">
  	<meta http-equiv="Last-Modified" content="0">
  	<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
  	<meta http-equiv="Pragma" content="no-cache">	
  	<script src="https://kit.fontawesome.com/f6540b2b09.js" crossorigin="anonymous"></script>	
</head>

<body>

	<header class="titulo">
		<h1>Clínica Sonrie</h1>
	</header>

	<ul class="menu">
		<li><a href="index.php">Inicio</a></li>
		<li><a href="encuentranos.php">Encuentranos</a></li>
		<li><a href="dentistas.php">Nuestros Dentistas</a></li>
		<li><a href="reservar.php">Reservar Hora</a></li>
		<li><a href="trabajos.php">Nuestros Trabajos</a></li>		
		<li><a href="inicio-sesion.php"><i class="fas fa-user" ></i></a></li>		
	</ul>	
		<!--
	<div class="cnt-form"> 

			<img src="img/descarga.png" alt="" class="logo">
			<h1 class="title">Iniciar Sesion</h1>

			<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"])?>" method="post" >
				<label for="">Email</label>
				<input type="text" name="email">
				<span class="msg-error"><?php  echo $email_err;  ?></span>
				<label for="">Contraseña</label>
				<input type="password" name="password">
				<span class="msg-error"><?php  echo $password_err; ?></span>
				<input type="submit" value="Iniciar">
			</form>

			<span class="text-footer">¿Aún no te has registrado?
				<a href="register.php">Registrate</a>
			</span>
		</div>
-->
<div class="container-all">

	<div class="cnt-form1">
  
		  <ul class="slider">
			    <li id="slide1">
			      <img src="img/Profesionales_v1.png" />
			    </li>
			    <li id="slide2">
			      <img src="img/Especialidades_v1.png"/>
			    </li>
			    <li id="slide3">
			      <img src="img/dentista.jpg"/>
			    </li>        
 		 </ul>
  	
  		<ul class="menu1">
		    <li>
		     	 <a href="#slide1">1</a>
		    </li>
		    <li>
		    	  <a href="#slide2">2</a>
   			 </li>
   			  <li>
   			 	  <a href="#slide3">3</a>
   			 </li>       
  		</ul>
 


	</div>
		<div class="capa-slider">
			<div class="capa"></div>
			<h1 class="title-description"></h1>
			<p class="text-description1">
				<a href="reservar.php">Conversa con nosotros, Pide tu hora AQUI</a>
			</p>
		</div>
</div>

		<div class="container-all">
				<div class="cnt-form"> 
				<img src="img/what.png" alt="" class="logo">
				<h1 class="title">Contactanos via WhatsApp </h1>
				<h1 class="title">+5973250854</h1>

			
				</div>


			<div class="ctn-text225">
				<div class="capa"></div>
				<h1 class="title-description"></h1>
				<p class="text-description">
					<a href="reservar.php"></a>
				</p>
			</div>
		</div>



		<div class="container-all">
			<div class="cnt-form"> 

			<img src="img/implantes.png" alt="" class="logo">
			<h1 class="title">Implantes </h1>
			<h1 class="title">Son estructuras de titanio que se insertan en el hueso mediante una cirugía ambulatoria.</h1>

			
		</div>


		<div class="ctn-text222">
			<div class="capa"></div>
			<h1 class="title-description"></h1>
			<p class="text-description">
				<a href="reservar.php"></a>
			</p>
		</div>
	</div>	

		<div class="container-all">
				<div class="cnt-form"> 
				<img src="img/vanguardia.png" alt="" class="logo">
				<h1 class="title">Vanguardia </h1>
				<h1 class="title">Se utilizan las mejores tecnologias disponibles de los mejores estandares a nivel regional.</h1>

			
				</div>


			<div class="ctn-text223">
				<div class="capa"></div>
				<h1 class="title-description"></h1>
				<p class="text-description">
					<a href="reservar.php"></a>
				</p>
			</div>
		</div>		


		<div class="container-all">
				<div class="cnt-form"> 
				<img src="img/ortodoncia.png" alt="" class="logo">
				<h1 class="title">Ortodoncia </h1>
				<h1 class="title">Especialidad dedicada al estudio, diagnóstico y corrección de las anomalías del crecimiento y desarrollo facial, a través del uso de aparatología.</h1>

			
				</div>


			<div class="ctn-text224">
				<div class="capa"></div>
				<h1 class="title-description"></h1>
				<p class="text-description">
					<a href="reservar.php"></a>
				</p>
			</div>
		</div>

<br>
    <br>
    <br>
    <footer class="footer">
        <div class="container-fot">
            <div class="row-fot">
                <div class="footer-col">
                    <h4>Clinica Sonrie</h4>
                    <ul>
                        <li><a href="#" target="_blank">Encuentranos</a></li>
                        <li><a href="#">Nuestros Dentistas</a></li>
                        <li><a href="#">Reservar Hora</a></li>
                    </ul>
                </div>

                <div class="footer-col">
                    <h4>Redes Sociales</h4>
                    <ul>
                        <li><a href="#" target="_blank">Pruebas</a></li>
                    </ul>
                </div>


            </div>
            <br>
                <div class="divsitio">
                    <h4 class="sitio_des">2020 - 2021. Sitio Desarrollado por BrandMans</h4>
                </div>
            </div>

    </footer>
  	
</body>




</html>
